#-------------------------------------------------
#
# Project created by QtCreator 2014-09-19T19:35:49
#
#-------------------------------------------------

QT       += core gui
QT	 += sql

QMAKE_CXXFLAGS += -std=c++0x -g

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DocArch
TEMPLATE = app
RC_FILE = res.rc

SOURCES += main.cpp\
        docarch.cpp \
        sqlite.cpp \
    document.cpp

HEADERS  += docarch.h \
        sqlite.h \
    document.h

FORMS    += docarch.ui

RESOURCES += \
    res.qrc
