#include "docarch.h"
#include "ui_docarch.h"

docArch::docArch(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::docArch)
{
    ui->setupUi(this);

    connect(ui->btnSearch, SIGNAL(clicked()), this, SLOT(searchDocs()));
    connect(ui->leSearch, SIGNAL(returnPressed()), this, SLOT(searchDocs()));

    connect(ui->btnAddDocument, SIGNAL(clicked()), this, SLOT(addDoc()));
    //connect(ui->lwResults, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(openDoc(QListWidgetItem*)));
    connect(ui->lwNewDocs, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(docChanged(QListWidgetItem*)));
    connect(ui->lwNewDocs, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(openNewDoc(QListWidgetItem*)));
    connect(ui->leNewTitle, SIGNAL(textChanged(const QString)), this, SLOT(enableAddButton()));
    connect(ui->leKeywords, SIGNAL(textChanged(const QString)), this, SLOT(enableAddButton()));
    connect(ui->pb_SaveSettings, SIGNAL(clicked()), this, SLOT(saveSettings()));
    connect(ui->pb_reload, SIGNAL(clicked()), this, SLOT(loadNewDocs()));
    connect(ui->pb_about, SIGNAL(clicked()), this, SLOT(showAbout()));

    //connect(ui->lwKeywords, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(kwChanged(QListWidgetItem*)));

    ui->leSearch->setFocus();
    ui->de_date->setDate(QDate::currentDate());

    QStringList header({"ID", "Title", "Doc-Date", "Imported", "Keywords"});
    ui->docTable->setHorizontalHeaderLabels(header);

    loadSettings();

    db = new SQLite(dbPath);
    db->init();

    loadNewDocs();
    loadKeywords();

    //required to store WindowSize on Resize
    resizeTimer.setSingleShot( true );
    connect( &resizeTimer, SIGNAL(timeout()), SLOT(resizeDone()) );

    ui->keywordTree->setCurrentItem(ui->keywordTree->topLevelItem(0));
}

docArch::~docArch()
{
    delete db;
    delete ui;
}


void docArch::loadKeywords(){
    ui->leKeywords->clear();
    ui->keywordTree->clear();


    QList<QString> keywordList = db->getKeyWords();

    QTreeWidgetItem *item = new QTreeWidgetItem();
    item->setText(0, "Alle");

    for(QString keyword : keywordList) {
        QTreeWidgetItem *ci = new QTreeWidgetItem();
        ci->setText(0, keyword.toUtf8());
        item->addChild(ci);
    }
    ui->keywordTree->addTopLevelItem(item);
    ui->keywordTree->expandAll();
}



void docArch::showAbout() {
    QString data = "Version 1.0.0<br>";
    data += "Icons made by:<br>";
    data += "<a href=\"http://www.freepik.com\" title=\"Freepik\">Freepik</a><br>";
    data += "<a href=\"http://RamiMcM.in\" title=\"Rami McMin\">Rami McMin</a><br>";
    data += "<a href=\"http://www.simpleicon.com\" title=\"SimpleIcon\">SimpleIcon</a><br>";
    data += "From <a href=\"http://www.flaticon.com\" title=\"Flaticon\">www.flaticon.com</a><br>";
    data += "Licensed under <a href=\"http://creativecommons.org/licenses/by/3.0/\" title=\"Creative Commons BY 3.0\">CC BY 3.0</a>";
    QMessageBox::information(this, "Über",data);
}

void docArch::on_docTable_cellDoubleClicked(int row, int column)
{
    QTableWidgetItem* i = ui->docTable->item(row, 1);
    Document d = db->DocByTitle(i->text());
    QDesktopServices::openUrl(QUrl::fromLocalFile(dir_allDocs.path() + "/" + d.getPath()));
}

void docArch::on_keywordTree_currentItemChanged(QTreeWidgetItem *current, QTreeWidgetItem *previous)
{
    if(!current)
        return;

    QList<Document> docs;
    if(current->text(0) == "Alle")
        docs = db->AllDocs();
    else
        docs = db->DocsByKeyword(current->text(0));

    updateDocList(docs);


}

void docArch::saveSettings() {

    QString folder = QStandardPaths::locate(QStandardPaths::GenericConfigLocation, QString(), QStandardPaths::LocateDirectory);
    folder += "docArch/";

    QFile settingsFile(folder + "docArch.ini");
    settingsFile.open(QIODevice::WriteOnly);
    QTextStream out(&settingsFile);
    out << ui->te_settings->toPlainText();
    settingsFile.close();

    loadSettings();

}


void  docArch::docChanged(QListWidgetItem* qlwi){
    ui->leNewTitle->setText( qlwi->text());
}

void  docArch::enableAddButton(){
    if(ui->leNewTitle->text().count() > 1 &&  ui->leKeywords->text().count() && ui->lwNewDocs->selectedItems().count() == 1) {
        ui->btnAddDocument->setEnabled(true);
    } else {
        ui->btnAddDocument->setEnabled(false);
    }
}


QString editLength(int minLength, QString input, QString extendWith)
{
    while(input.size() < minLength) {
        input = extendWith + input;
    }

    return input;
}




void  docArch::addDoc(){
    QString keywordString = ui->leKeywords->text();
    QStringList keywordList =  keywordString.split(",", QString::SkipEmptyParts);

    QListWidgetItem *qlwi = ui->lwNewDocs->selectedItems()[0];
    QString sourceFilename = qlwi->text();

    Document *d = new Document();
    d->Title = ui->leNewTitle->text();
    d->Date = ui->de_date->date();
    d->Imported = QDate::currentDate();
    d->Filename = sourceFilename;
    bool ok = db->addDocument(d, keywordList);

    if(!ok)
        return;


    QString folderName = d->getFolderName();
    QString sourcePath(dir_newDocs.path() + "/" + sourceFilename);

    QDir dest (dir_allDocs.path() + "/" + folderName);
    if(!dest.exists()) dest.mkdir(dest.path());

    //generate DesinationPath
    QString  destinationFile(dir_allDocs.path() + "/" + folderName + "/" + d->getFilePrefix() + d->Filename);

    if(!QFile::copy(sourcePath, destinationFile)) {
        QMessageBox::critical(this,"Error", "Fehler beim Kopieren!");
        return;
    }

    if(!QFile::remove(sourcePath)) {
        QMessageBox::critical(this,"Error", "Fehler beim löschen!");
        return;
    }

    ui->leKeywords->setText("");
    ui->leNewTitle->setText("");
    int ID = d->ID;
    delete d;

    loadNewDocs();
    loadKeywords();


    QMessageBox::information(this, "Document added!", "Document added with ID "  + QString::number(ID));
}



void  docArch::openNewDoc(QListWidgetItem* qlwi){
    ui->leNewTitle->setText(qlwi->text());
    QString pdfFile = dir_newDocs.path() + "/" +  qlwi->text();
    QDesktopServices::openUrl(QUrl::fromLocalFile(pdfFile));
}

void  docArch::loadSettings() {
    QString folder = QStandardPaths::locate(QStandardPaths::GenericConfigLocation, QString(), QStandardPaths::LocateDirectory);
    folder += "docArch/";

    QDir settingsFolder(folder);
    if(!settingsFolder.exists()) {
        settingsFolder.mkdir(folder);
        qDebug() << "Ordner erstellt";
    }

    QFile settingsFile(folder+"docArch.ini");
    QSettings *settings = new QSettings(folder+"docArch.ini", QSettings::IniFormat);
    if(!settingsFile.exists()) {
        qDebug() << "Datei nicht gefunden";
        //load Defaults
        QString stdFolder = QStandardPaths::locate(QStandardPaths::DocumentsLocation, QString(), QStandardPaths::LocateDirectory);
        dbPath = stdFolder + "docArch.db";
        dir_newDocs = stdFolder + "new";
        dir_allDocs = stdFolder;
        windowSize = QSize(500,500);

        settings->setValue("dbPath", dbPath);
        settings->setValue("dir_newDocs", dir_newDocs.path());
        settings->setValue("dir_allDocs", dir_allDocs.path());
        settings->setValue("windowSize", windowSize);
        settings->sync();
        qDebug() << "Datei erstellt";

    } else {
        dbPath = settings->value("dbPath", "").toString();
        dir_newDocs = settings->value("dir_newDocs", "").toString();
        dir_allDocs = settings->value("dir_allDocs", "").toString();
        windowSize = settings->value("windowSize").toSize();
        this->resize(windowSize);
    }

    settingsFile.open(QIODevice::ReadOnly);
    QTextStream in(&settingsFile);
    ui->te_settings->clear();

    while(!in.atEnd()) {
        QString line = in.readLine();
        ui->te_settings->appendPlainText(line);
    }

    settingsFile.close();
}

void  docArch::loadNewDocs(){
    ui->lwNewDocs->clear();
    dir_newDocs.refresh();
    QStringList newFiles = dir_newDocs.entryList();
    for(QString entry : newFiles) {
        ui->lwNewDocs->addItem(entry);
    }
}


void docArch::searchDocs(){
    QString keyword = ui->leSearch->text();
    QList<Document> docs = db->searchDocuments(keyword);
    updateDocList(docs);
}


void docArch::updateDocList(QList<Document> docs) {
    ui->docTable->clearContents();
    ui->docTable->setRowCount(docs.length());
    int i = 0;
    for(Document d : docs) {

        QString keyList;
        for(QString k: d.Keywords)
            keyList += k + ", ";


        ui->docTable->setItem(i, 0, new QTableWidgetItem(QString::number(d.ID)));
        ui->docTable->setItem(i, 1, new QTableWidgetItem(d.Title));
        ui->docTable->setItem(i, 2, new QTableWidgetItem(d.Date.toString()));
        ui->docTable->setItem(i, 3, new QTableWidgetItem(d.Imported.toString()));
        ui->docTable->setItem(i, 4, new QTableWidgetItem(keyList));
        ++i;
    }
}

void docArch::resizeEvent(QResizeEvent* event)
{
    //This event Fires everytime, the window is resized by a pixel.
    //using a timer to get some delay  & not flooding the DB.
    resizeTimer.start( 500 );
   QMainWindow::resizeEvent(event);
}




void docArch::resizeDone()
{
    windowSize = this->size();
 //   db.updateIntPref("minviewWidth", this->width());
   // db.updateIntPref("minviewHeight", this->height());

    QString folder = QStandardPaths::locate(QStandardPaths::GenericConfigLocation, QString(), QStandardPaths::LocateDirectory);
    folder += "docArch/";

    QDir settingsFolder(folder);
    if(!settingsFolder.exists()) {
        return;
    }

    QFile settingsFile(folder+"docArch.ini");
    QSettings *settings = new QSettings(folder+"docArch.ini", QSettings::IniFormat);
    if(!settingsFile.exists()) {
        return;
    } else {
        settings->setValue("windowSize", windowSize);
        settings->sync();
        qDebug() << "Wrote Size" << windowSize;
    }

    settingsFile.open(QIODevice::ReadOnly);
    QTextStream in(&settingsFile);
    ui->te_settings->clear();

    while(!in.atEnd()) {
        QString line = in.readLine();
        ui->te_settings->appendPlainText(line);
    }

    settingsFile.close();
}
