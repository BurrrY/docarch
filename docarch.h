#ifndef DOCARCH_H
#define DOCARCH_H

#include "sqlite.h"

#include <QMainWindow>
#include <QMessageBox>
#include <QDesktopServices>
#include <QStandardPaths>
#include <QSettings>
#include <QListWidgetItem>
#include <QDir>
#include <QFile>
#include <QtDebug>
#include <string.h>
#include <map>
#include <QTreeWidget>
#include <QTimer>

namespace Ui {
class docArch;
}

class docArch : public QMainWindow
{
    Q_OBJECT

public:
    explicit docArch(QWidget *parent = 0);
    ~docArch();

private:
    Ui::docArch *ui;
    void loadSettings(void);
    void loadKeywords(void);

    QDir dir_allDocs;
    QDir dir_newDocs;
    QString dbPath;
    QSize windowSize;
    SQLite *db;
    void updateDocList(QList<Document> docs);

    //Save GUI-Size on resize
    void resizeEvent(QResizeEvent *event);
    QTimer resizeTimer;
private slots:
    void saveSettings();
    void searchDocs();
    void docChanged(QListWidgetItem* qlwi);
    void enableAddButton();
    void openNewDoc(QListWidgetItem* qlwi);
    void addDoc();
    void loadNewDocs(void);
    void showAbout(void);

    void on_docTable_cellDoubleClicked(int row, int column);
    void on_keywordTree_currentItemChanged(QTreeWidgetItem * current, QTreeWidgetItem * previous);


    void resizeDone();
};

#endif // DOCARCH_H
