#include "document.h"

Document::Document()
{

}

QString Document::getFolderName()
{
    //Generate Folder-name
    //input: 				235423  //23
    int min = (ID/100); //= 2354--  //0
    min *= 100; 		//= 235400  //0
    int max = min+99;   //= 235499  //99


    QString buffMin;
    buffMin.sprintf("%03d", min);

    QString buffMax;
    buffMax.sprintf("%03d", max);

    return "ID_" + buffMin + "-" +  buffMax;

}

QString Document::getPath()
{
    QString buffFile;
    buffFile.sprintf("%05d", ID);

//    string pdfFile = dir_allDocs.path().toStdString() + "/ID_" + buffMin + "-" + buffMax + "/" + buffFile + "_"  + result[0][0].toStdString();
    return getFolderName() + "/" + buffFile + "_"  + Filename;
}

QString Document::getFilePrefix()
{
    QString buffFile;
    buffFile.sprintf("%05d", ID);
    return buffFile + "_";
}

