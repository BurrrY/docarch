#ifndef DOCUMENT_H
#define DOCUMENT_H

#include <QDate>
#include <QString>
#include <QStringList>



class Document
{
public:
    Document();

    int ID;
    QString Title;
    QString Filename;
    QStringList Keywords;
    QDate Imported;
    QDate Date;

    QString getFolderName();
    QString getPath();
    QString getFilePrefix();

    friend bool operator==(const  Document &g1, const Document &g2)
    {
       return (g1.Title==g2.Title && g1.ID==g2.ID);
    }
};

#endif // DOCUMENT_H
