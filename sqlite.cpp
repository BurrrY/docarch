#include "sqlite.h"

#include <QSqlError>

SQLite::SQLite(QString dbPath)
{
    _dbPath = dbPath;
}

QList<QString> SQLite::getKeyWords()
{
    QList<QString> res;
    QSqlQuery query;
    query.exec("SELECT kw_Word FROM keywords ORDER BY kw_word ASC");
    while(query.next())
        if(query.value(0).toString().size()>0)
            res.append(query.value(0).toString());

    return res;
}

QList<Document> SQLite::DocsByKeyword(QString Keyword)
{
    QList<Document> res;
    QSqlQuery query;
    query.exec(("SELECT docs.do_ID, docs.do_Name, do_date, do_imported FROM docs, docKeywords, keywords WHERE docs.do_ID = docKeywords.dk_Doc AND docKeywords.dk_Keywords = keywords.kw_ID AND keywords.kw_Word = '" + Keyword + "'"));

    while(query.next()) {
        Document d;
        d.ID = query.value(0).toInt();
        d.Title = query.value(1).toString();
        d.Keywords = getKeywords(d.ID);
        d.Date = query.value(2).toDate();
        d.Imported = query.value(3).toDate();
        res.append(d);
    }

    return res;
}

QList<Document> SQLite::AllDocs()
{
    QList<Document> res;
    QSqlQuery query;
    query.exec("SELECT docs.do_ID, docs.do_Name, docs.do_date, docs.do_imported FROM docs");

    while(query.next()) {
        Document d;
        d.ID = query.value(0).toInt();
        d.Title = query.value(1).toString();
        d.Keywords = getKeywords(d.ID);
        d.Date = query.value(2).toDate();
        d.Imported = query.value(3).toDate();
        res.append(d);
    }

    return res;
}

QStringList SQLite::getKeywords(int docID)
{
    QStringList res;
    QSqlQuery query;
    query.prepare("SELECT kw_Word FROM docKeywords, keywords WHERE docKeywords.dk_Keywords = keywords.kw_ID AND docKeywords.dk_Doc = :docID");
    query.bindValue(":docID", docID);
    query.exec();

    while(query.next())
        res.append(query.value(0).toString());


    return res;
}



QList<Document> SQLite::searchDocuments(QString keyword)
{
    QList<Document> docs;

    QSqlQuery query;
    query.prepare("SELECT do_ID, do_Name, do_date, do_imported FROM docs WHERE do_Name LIKE '%'||:keyword||'%'");
    query.bindValue(":keyword", keyword);
    query.exec();
    while(query.next()) {
        Document d;
        d.ID = query.value(0).toInt();
        d.Title = query.value(1).toString();
        d.Keywords = getKeywords(d.ID);
        d.Date = query.value(2).toDate();
        d.Imported = query.value(3).toDate();
        if(!docs.contains(d))
            docs.append(d);
    }

    QSqlQuery q;
    q.prepare("SELECT docs.do_ID, docs.do_Name, do_date, do_imported FROM docs, docKeywords, keywords WHERE docs.do_ID = docKeywords.dk_Doc AND docKeywords.dk_Keywords = keywords.kw_ID AND keywords.kw_Word LIKE '%'||:keyword||'%'");
    q.bindValue(":keyword", keyword);
    q.exec();
    while(q.next()) {
        Document d;
        d.ID = q.value(0).toInt();
        d.Title = q.value(1).toString();
        d.Keywords = getKeywords(d.ID);
        d.Date = q.value(2).toDate();
        d.Imported = q.value(3).toDate();
        if(!docs.contains(d))
            docs.append(d);
    }

    return docs;
}

Document SQLite::DocByTitle(QString title)
{
    QSqlQuery q;
    q.prepare("SELECT docs.do_File, docs.do_ID, do_Name, do_date, do_imported FROM docs WHERE docs.do_Name = :docTitle");
    q.bindValue(":docTitle", title);
    q.exec(); q.next();

    Document d;
    d.Filename = q.value(0).toString();
    d.ID = q.value(1).toInt();
    d.Title = q.value(2).toString();
    d.Date = q.value(3).toDate();
    d.Imported = q.value(4).toDate();
    return d;
}


bool SQLite::tryExecute(QSqlQuery *q) {
    bool queryOK = q->exec();

    if (!queryOK)
    {
        QString queryStr = q->lastQuery();
        QSqlError e = q->lastError();

        QString boundValues;
        QMapIterator<QString, QVariant> i(q->boundValues());
        while (i.hasNext())
        {
            i.next();
            boundValues += i.key() + ": '" + i.value().toString() + "'; ";
        }

        qDebug() << ("####################");
        qDebug() << (e.databaseText());
        qDebug() << (e.driverText());
        qDebug() << (queryStr);
        qDebug() << (boundValues);
    }

    return queryOK;
}

bool SQLite::addDocument(Document *d, QStringList keywordList)
{
    QSqlQuery query;
    query.prepare("INSERT INTO docs (do_Name, do_File, do_date, do_imported) VALUES (:title, :filename, :date, :imported)");
    query.bindValue(":title", d->Title);
    query.bindValue(":filename", d->Filename);
    query.bindValue(":date", d->Date.toString(DB_DATE_FORMAT));
    query.bindValue(":imported", d->Imported.toString(DB_DATE_FORMAT));
    bool ok = tryExecute(&query);

    if(!ok)
        return false;

    int ID = getLatestID();
    d->ID = ID;

//INSERTING KEYWOrDS, connect the to the Doc
    for(QString s: keywordList)
        addKeyword(ID, s.trimmed());
}

bool SQLite::addKeyword(int DocumentID, QString Keyword) {

    int keywordID;

    //Check if Keyword exists
    QSqlQuery query;
    query.prepare("SELECT kw_id FROM keywords WHERE kw_Word = :keyword");
    query.bindValue(":keyword", Keyword);
    query.exec();

    if(!query.next()) //Does not exists, add
    {
        QSqlQuery q;
        q.prepare("INSERT INTO keywords (kw_Word) VALUES (:keyword)");
        q.bindValue(":keyword", Keyword);
        q.exec();


        q.prepare("SELECT kw_ID FROM keywords WHERE kw_Word = :keyword");
        q.bindValue(":keyword", Keyword);
        q.exec();
        q.next();
        keywordID = q.value(0).toInt();
        qDebug() << "Keyword added to DB: " << Keyword;
    }
    else
    {
        keywordID = query.value(0).toInt();
    }


    QSqlQuery insertQuery;
    insertQuery.prepare("INSERT INTO docKeywords (dk_Doc, dk_Keywords) VALUES(:docID, :keywordID)");
    insertQuery.bindValue(":docID", DocumentID);
    insertQuery.bindValue(":keywordID", keywordID);
    insertQuery.exec();
}

int SQLite::getLatestID()
{
    QSqlQuery query;
    query.exec("SELECT do_ID FROM docs ORDER BY do_ID DESC");
    query.next();
    return query.value(0).toInt();
}

bool SQLite::init()
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setHostName("localhost");
    db.setDatabaseName(_dbPath);
    bool ok = db.open();

    if(!ok)
        return false;

    QSqlQuery query;
    query.exec("CREATE TABLE IF NOT EXISTS keywords (kw_ID INTEGER PRIMARY KEY, kw_Word TEXT);");
    query.exec("CREATE TABLE IF NOT EXISTS docs (do_ID INTEGER PRIMARY KEY, do_Name TEXT, do_File TEXT);");
    query.exec("CREATE TABLE IF NOT EXISTS docKeywords (dk_ID INTEGER PRIMARY KEY, dk_Doc NUMERIC, dk_Keywords NUMERIC);");

    return true;
}
