#ifndef SQLITE_H
#define SQLITE_H


#include "document.h"

#include <QList>
#include <QVariant>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDebug>


#define DB_DATE_FORMAT "yyyy-MM-dd"
class SQLite
{
public:
    SQLite(QString dbPath);
    QList<QString> getKeyWords();

    QList<Document> DocsByKeyword(QString Keyword);
    QList<Document> searchDocuments(QString keyword);
    Document DocByTitle(QString title);

    bool addDocument(Document *d, QStringList keywordList);
    int getLatestID();
    bool addKeyword(int DocumentID, QString Keyword);
    bool init();
    QList<Document> AllDocs();
private:
    QSqlDatabase db;
    QString _dbPath;
    QStringList getKeywords(int docID);
    bool tryExecute(QSqlQuery *q);
};

#endif // SQLITE_H
